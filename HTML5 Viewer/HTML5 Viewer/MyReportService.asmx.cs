﻿using System;
using System.IO;
using System.Web.Services;
using System.Xml;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Data;
using GrapeCity.ActiveReports.SectionReportModel;
using GrapeCity.ActiveReports.Web;
using System.Web;


namespace HTML5Viewer
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    public class MyReportService : ReportService
    {
        protected override object OnCreateReportHandler(string reportPath)
        {

            if (!string.IsNullOrEmpty(reportPath))
            {
                
                if (reportPath.Contains(".rdlx"))
                {
                    var path = HttpContext.Current.Server.MapPath(reportPath);
                    PageReport pageReport = new PageReport();
                    pageReport.ResourceLocator = new ActiveReportResourceLocator(); ;
                    using (var stream = File.OpenRead(path)) 
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            pageReport.Load(reader);
                        }
                    }
                    // html export test
                    //var htmlre = new GrapeCity.ActiveReports.Export.Html.Page.HtmlRenderingExtension();
                    //var pd = new GrapeCity.ActiveReports.Document.PageDocument(pageReport);
                    //var msp = new GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider();
                    //pd.Render(htmlre, msp);
                    //pdf test
                    //var pdfre = new GrapeCity.ActiveReports.Export.Pdf.Page.PdfRenderingExtension();
                    //var pd = new GrapeCity.ActiveReports.Document.PageDocument(pageReport);
                    //var msp = new GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider();
                    //var settings = new GrapeCity.ActiveReports.Export.Pdf.Page.Settings();
                    //settings.FallbackFonts = "Arial";
                    //pd.Render(pdfre, msp, settings);
                    
                    return pageReport;
                }                
            }
            return base.OnCreateReportHandler(reportPath);
        }
    }
}