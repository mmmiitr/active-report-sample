﻿using GrapeCity.ActiveReports.Extensibility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace HTML5Viewer
{
    public class ActiveReportResourceLocator : ResourceLocator
    {
        public override Resource GetResource(ResourceInfo resourceInfo)
        {
            Resource resource;
            string name = resourceInfo.Name;
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Resource Name Is Null", "resourceInfo");
            }
            else if (name.Contains(".rdlx-master"))
            {
                //name = name.Replace("..\\", "");
            }
            var path = HttpContext.Current.Server.MapPath("Reports\\master.rdlx-master");
            var stream = File.OpenRead(path);
            Uri uri = new Uri(path);
            resource = new Resource(stream, uri);
            return resource;
        }
    }
}